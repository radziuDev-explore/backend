import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.alterTable('offer', (table: Knex.AlterTableBuilder) => {
    table.dropColumn('tag');
    table.integer('tagId').notNullable().references('tag.id').onDelete('CASCADE');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.alterTable('offer', (table: Knex.AlterTableBuilder) => {
    table.integer('tag').notNullable();
    table.dropColumn('tagId');
  });
}