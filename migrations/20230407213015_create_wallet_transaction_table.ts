import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('wallet_transaction', (table: Knex.AlterTableBuilder) => {
    table.increments('id');
    table.integer('userId').unsigned().notNullable().references('user.id');
    table.integer('value').notNullable();
    table.integer('balance').unsigned().notNullable();
    table.string('location', 32).notNullable();
    table.json('meta').nullable();
    table.dateTime('createdAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
    table.dateTime('updatedAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
  });
};

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('wallet_transaction');
};