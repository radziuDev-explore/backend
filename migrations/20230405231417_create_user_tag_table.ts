import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('userTag', (table: Knex.AlterTableBuilder) => {
    table.increments('id');
    table.integer('userId').notNullable().references('user.id').onDelete('CASCADE');
    table.integer('tagId').notNullable().references('tag.id');    
    table.dateTime('updatedAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
    table.dateTime('createdAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('userTag');
}