import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.alterTable('tag', (table: Knex.AlterTableBuilder) => {
    table.string('tag', 100).unique().notNullable().alter();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.alterTable('tag', (table: Knex.AlterTableBuilder) => {
    table.dropUnique(['tag'])
  });
}

