import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('wallet', (table: Knex.AlterTableBuilder) => {
    table.increments('id');
    table.integer('userId').unique().notNullable().references('user.id').onDelete('CASCADE');
    table.integer('points');
    table.dateTime('updatedAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
    table.dateTime('createdAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('wallet');
}

