import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.alterTable('userTag', (table: Knex.AlterTableBuilder) => {
    table.unique(['tagId', 'userId']);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.alterTable('userTag', (table: Knex.AlterTableBuilder) => {
    table.dropUnique(['tagId', 'userId']);
  });
}

