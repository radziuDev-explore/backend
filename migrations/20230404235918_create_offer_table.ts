import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('offer', (table: Knex.AlterTableBuilder) => {
    table.increments('id');
    table.string('tag', 100).notNullable();
    table.string('title', 500).notNullable();
    table.string('link', 500).notNullable();
    table.integer('price', 500).notNullable();
    table.dateTime('updatedAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
    table.dateTime('createdAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('offer');
}

