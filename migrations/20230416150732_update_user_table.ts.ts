import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.alterTable('user', (table: Knex.AlterTableBuilder) => {
    table.string('name').notNullable().unique();
    table.string('email').notNullable().unique();
    table.string('password');
    table.dateTime('confirmedAt');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.alterTable('user', (table: Knex.AlterTableBuilder) => {
    table.dropColumn('name');
    table.dropColumn('email');
    table.dropColumn('password');
    table.dropColumn('confirmedAt');
  });
}

