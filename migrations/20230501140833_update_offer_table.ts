import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.alterTable('offer', (table: Knex.AlterTableBuilder) => {
    table.string('title', 1000).notNullable().alter();
    table.string('link', 1000).notNullable().alter();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.alterTable('offer', (table: Knex.AlterTableBuilder) => {
    table.string('title', 500).notNullable().alter();
    table.string('link', 500).notNullable().alter();
  });
}

