import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.alterTable('userTag', (table: Knex.AlterTableBuilder) => {
    table.integer('quantityOfPages').notNullable().defaultTo(1);
    table.json('meta').notNullable().defaultTo({ services: ['amazon', 'allegro' ] });
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.alterTable('userTag', (table: Knex.AlterTableBuilder) => {
    table.dropColumn('quantityOfPages');
    table.dropColumn('meta');
  });
}

