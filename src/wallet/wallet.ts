import { InsufficientFundException } from './exception/insufficient-fund.exception';

type WalletProps = {
  userId: number;
  points: number;
  createdAt: Date;
  updatedAt: Date;
};

export class Wallet {
  private userId: number;
  private points: number;
  private createdAt: Date;
  private updatedAt: Date;
  
  constructor(props: WalletProps) {
    this.userId = props.userId;
    this.points = props.points;
    this.createdAt = props.createdAt;
    this.updatedAt = props.updatedAt;
  }

  getBalance() {
    return this.points;
  }

  add(value: number): void {
    this.points += value;
  }

  subtract(value: number): void {
    if (this.points < value) {
      throw new InsufficientFundException();
    }

    this.points -= value;
  }

  snapshot() {
    return {
      userId: this.userId,
      points: this.points,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }
};