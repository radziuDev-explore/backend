import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { WalletTransactionLocation } from './enum/wallet-transaction-location.enum';
import { WalletUpdatedEvent } from './event/wallet-updated.event';
import { Wallet } from './wallet';
import { WalletRepository } from './infrastructure/wallet.repository';

@Injectable()
export class WalletService {
  constructor(
    private readonly walletRepository: WalletRepository,
    private readonly eventEmitter: EventEmitter2,
  ) { }

  public async add(
    userId: number,
    value: number,
    location: WalletTransactionLocation,
    meta?: Record<string, any>,
  ): Promise<void> {
    await this.updateWalletBalance(
      'add',
      userId,
      value,
      location,
      meta,
    );
  }

  public async subtract(
    userId: number,
    value: number,
    location: WalletTransactionLocation,
    meta?: Record<string, any>,
  ): Promise<void> {
    await this.updateWalletBalance(
      'subtract',
      userId,
      value,
      location,
      meta,
    );
  }

  public async createWalletForUser(
    userId: number,
  ): Promise<void> {
    await this.walletRepository.create(userId)
  }

  private async updateWalletBalance(
    mode: 'add' | 'subtract',
    userId: number,
    value: number,
    location: WalletTransactionLocation,
    meta?: Record<string, any>,
  ): Promise<Wallet> {
    const { wallet, trx } = await this.walletRepository.findForUpdateByUserId(userId, true);
    const transactionValue = mode === 'add' ? value : -value;
    
    try {
      mode === 'add'
      ? wallet.add(value)
      : wallet.subtract(value);
      
      await this.walletRepository.save(wallet, trx);
      
      const transactionId = await this.walletRepository.saveTransaction(
        userId,
        transactionValue,
        wallet.getBalance(),
        location,
        meta,
        trx,
      );
        
      const transaction = {
        id: transactionId,
        value: transactionValue,
        location,
        meta,
      };

      this.eventEmitter.emit('wallet.updated',
        new WalletUpdatedEvent(
          wallet.snapshot(),
          transaction,
        ),
      );

      await trx.commit();
    } catch (error) {
      await trx.rollback();
      throw error;
    }
    
    return wallet;
  }
};