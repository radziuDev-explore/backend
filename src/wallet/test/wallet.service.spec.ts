import { Test, TestingModule } from '@nestjs/testing';
import { WalletService } from '../wallet.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { WalletRepository } from '../infrastructure/wallet.repository';
import { KnexModule } from 'nestjs-knex';

import Config from '../../config';

describe('WalletService', () => {
  let service: WalletService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        EventEmitterModule.forRoot(),
        KnexModule.forRoot({ config: Config.Database }),
      ],
      providers: [
        WalletService,
        WalletRepository,
      ],
    }).compile();

    service = module.get<WalletService>(WalletService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
