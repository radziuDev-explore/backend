import { Module } from '@nestjs/common';
import { WalletService } from './wallet.service';
import { WalletRepository } from './infrastructure/wallet.repository';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { UserCreatedListener } from './listener/user-created.listener';

@Module({
  providers: [
    EventEmitterModule,
    WalletRepository,
    WalletService,
    UserCreatedListener,
  ],
  exports: [
    WalletService,
  ],
})
export class WalletModule {};