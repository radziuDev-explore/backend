import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { WalletTransactionLocation } from '../enum/wallet-transaction-location.enum';
import { Wallet } from '../wallet';

@Injectable()
export class WalletRepository {
  public constructor(
    @InjectKnex() private readonly db: Knex,
  ) { }

  public async findForUpdateByUserId(
    userId: number,
    createIfNotExists: boolean = false,
  ): Promise<{
    wallet: Wallet,
    trx: any,
  }> {
    const trx = await this.db.transaction();

    const walletQuery = this.db('wallet')
      .transacting(trx)
      .forUpdate()
      .where('userId', userId)
      .first();

    let props = await walletQuery;

    if (!props && createIfNotExists) {
      await this.create(userId, trx);
      props = await walletQuery; 
    }

    const wallet = new Wallet(props);
    
    return {
      wallet,
      trx,
    };
  }
  
  public async create(userId: number, trx?: any): Promise<void> {
    const props = {
      userId,
      points: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    const query = this.db('wallet').insert(props);

    if (trx) {
      query.transacting(trx);
    }
    
    await query;
  }
  
  public async save(wallet: Wallet, trx?: any) {
    const snapshot = wallet.snapshot();

    const query = this.db('wallet')
      .where('userId', snapshot.userId)
      .update({
        points: snapshot.points,
        updatedAt: new Date(),
      });
    
    if (trx) {
      query.transacting(trx);
    }

    await query;
  }
  
  public async saveTransaction(
    userId: number,
    value: number,
    balance: number,
    location: WalletTransactionLocation,
    meta?: Record<string, string | number>,
    trx?: any,
  ): Promise<number> {
    const query = this.db('wallet_transaction')
      .insert({
        userId,
        value,
        balance,
        location,
        meta: meta && JSON.stringify(meta) || null,
      });
      
    if (trx) query.transacting(trx);

    const [{ transactionId }] = await query.returning('id');
    return transactionId;
  }
}
