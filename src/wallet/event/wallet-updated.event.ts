import { WalletTransactionLocation } from '../enum/wallet-transaction-location.enum';

export class WalletUpdatedEvent {
  constructor (
    public readonly wallet: {
      userId: number,
      points: number,
    },
    
    public readonly transaction: {
      id: number,
      value: number,
      location: WalletTransactionLocation,
      meta: Record<string, string | number>,
    },
  ) { }
};