import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { WalletService } from '../wallet.service';
import { UserCreatedEvent } from '../../user/event/user-created.event';

@Injectable()
export class UserCreatedListener {
  public readonly logger = new Logger(UserCreatedListener.name);

  constructor(
    private readonly walletService: WalletService,
  ) {}

  @OnEvent('user.created')
  async handleTagScrapedEvent(payload: UserCreatedEvent) {
    try {
      await this.walletService.createWalletForUser(payload.User.id);
    } catch (error) {
      this.logger.error('Creating wallet for user faild. Error: ', error);
    }
  }
}
