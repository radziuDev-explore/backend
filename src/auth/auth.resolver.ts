import { Resolver, Args, Mutation, Context } from '@nestjs/graphql';
import { InternalServerErrorException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthTokenDto } from './dto/auth-token.dto';
import { LoginUserInput } from './dto/login-user.input';

import Config from '../config'

@Resolver()
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation(() => AuthTokenDto)
  async login(
    @Args('loginUserInput') loginUserInput: LoginUserInput,
    @Context() context: { res: any },
  ): Promise<AuthTokenDto> {
    const subject = await this.authService.validateUser(loginUserInput);

    const { jwtToken, refreshToken } = await this.authService.createAuthTokens(subject);

    context.res.cookie('accessToken', jwtToken.token, jwtToken.cookieOptions);
    context.res.cookie('accessTokenType', Config.Auth.accessTokenType, jwtToken.cookieOptions);
    context.res.cookie('refreshToken', refreshToken.token, refreshToken.cookieOptions);

    return { jwtToken, refreshToken };
  }

  @Mutation(() => AuthTokenDto)
  async refreshToken(
    @Context() context: { res: any, req: any },
  ): Promise<AuthTokenDto> {
    const refreshTokenFromCookies = context.req.cookies['refreshToken'];
    const subject = await this.authService.checkRefreshTokenSubject(refreshTokenFromCookies);

    const { jwtToken, refreshToken } = await this.authService.createAuthTokens(subject);

    context.res.cookie('accessToken', jwtToken.token, jwtToken.cookieOptions);
    context.res.cookie('accessTokenType', Config.Auth.accessTokenType, jwtToken.cookieOptions);
    context.res.cookie('refreshToken', refreshToken.token, refreshToken.cookieOptions);

    return { jwtToken, refreshToken };
  }

  @Mutation(() => Boolean)
  async revokeRefreshToken(
    @Context() context: { res: any, req: any },
  ): Promise<Boolean> {
    try {
      const refreshTokenFromCookies = context.req.cookies['refreshToken'];
      await this.authService.revokeRefreshToken(refreshTokenFromCookies);
      return true;
    } catch (error) {
      console.log(error)
      throw new InternalServerErrorException('An error occurred while logging out.');
    }
  }
}