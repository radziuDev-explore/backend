import { InputType, Field } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

@InputType()
export class LoginUserInput {
  @Field()
  @IsNotEmpty({ message: "Field 'email' should not be empty." })
  @IsEmail({}, { message: "Field 'email' should be email." })
  email: string;

  @Field()
  @IsNotEmpty({ message: "Field 'password' should not be empty." })
  @IsString({ message: "Field 'password' should be string." })
  @Length(8, 40, { message: "Field 'password' should be between 8 and 40 characters long." })
  password: string;
}
