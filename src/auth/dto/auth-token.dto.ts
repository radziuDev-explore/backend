import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
class CookieOptions {
  @Field()
  httpOnly: boolean;

  @Field()
  sameSite: boolean;

  @Field()
  expires: Date;
}

@ObjectType()
class Token {
  @Field()
  token: string;

  @Field()
  expiresAt: Date;

  @Field(() => CookieOptions)
  cookieOptions: CookieOptions;
}

@ObjectType()
export class AuthTokenDto {
  @Field(() => Token)
  jwtToken: Token;

  @Field(() => Token)
  refreshToken: Token;
}