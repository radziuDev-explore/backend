import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { UserRepository } from './infrastructure/user.repository';
import { AuthResolver } from './auth.resolver';
import { JwtStrategy } from './strategies/jwt-strategy';
import { RefreshTokenRepository } from './infrastructure/refresh-token.repository';

import Config from '../config';

@Module({
  imports: [
    JwtModule.register({
      secret: Config.Auth.jwtSecret,
      signOptions: {
        expiresIn: `${Config.Auth.jwtExpire}s`
      },
    }),
  ],
  providers: [
    AuthService,
    UserRepository,
    RefreshTokenRepository,
    AuthResolver,
    JwtStrategy,
  ],
})

export class AuthModule {}
