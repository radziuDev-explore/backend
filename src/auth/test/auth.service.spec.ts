require('dotenv').config({ path: `${process.cwd()}/.env` });

import { Test, TestingModule } from '@nestjs/testing';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { UnauthorizedException } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { KnexModule } from 'nestjs-knex';
import { RefreshTokenRepository } from '../infrastructure/refresh-token.repository';
import { UserRepository } from '../infrastructure/user.repository';
import { CryptoService } from '../../crypto/crypto.service';
import { JwtStrategy } from '../strategies/jwt-strategy';
import { AuthService } from '../auth.service';
import { AuthResolver } from '../auth.resolver';
import * as argon2 from 'argon2';

import Config from '../../config';

describe('AuthService', () => {
  let service: AuthService;
  let repository: UserRepository;

  const correctUser = {
    email: 'correct@tester.com',
    password: 'admin123',
  };

  const incorrectUser = {
    email: 'undefined@tester.com',
    password: 'admin123',
  };
  
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        EventEmitterModule.forRoot(),
        KnexModule.forRoot({ config: Config.Database }),
        JwtModule.register({
          secret: Config.Auth.jwtSecret,
          signOptions: {
            expiresIn: `${Config.Auth.jwtExpire}s`
          },
        }),
      ],
      providers: [
        CryptoService,
        AuthService,
        UserRepository,
        AuthResolver,
        RefreshTokenRepository,
        JwtStrategy,
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    repository = module.get<UserRepository>(UserRepository);

    await repository.db('user')
      .insert({
        ...correctUser,
        name: "tester",
        password: await argon2.hash(correctUser.password)
      });
    await repository.db('user').where('email', incorrectUser.email).del();
  });

  afterEach(async() => {
    await repository.db('user').where('email', correctUser.email).del();
  });

  describe('login', () => {
    it('should return an AuthToken object if valid login credentials are provided', async () => {
      const authResult = await service.createAuthTokens(correctUser);

      expect(authResult).toHaveProperty('jwtToken');
      expect(authResult).toHaveProperty('refreshToken');
    });
  });

  describe('validateUser', () => {
    it('should return a Partial<User> object with user information if valid login credentials are provided', async () => {
      const validatedUser = await service.validateUser(correctUser);

      expect(validatedUser).toBeDefined();
      expect(validatedUser).not.toHaveProperty('password');
    });

    it('should throw an UnauthorizedException if invalid login credentials are provided', async () => {
      await expect(service.validateUser(incorrectUser))
        .rejects.toThrow(UnauthorizedException);
    });
  });
});
