require('dotenv').config({ path: `${process.cwd()}/.env` });

import { Test, TestingModule } from '@nestjs/testing';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { JwtModule } from '@nestjs/jwt';
import { KnexModule } from 'nestjs-knex'
import { AuthResolver } from '../auth.resolver';
import { JwtStrategy } from '../strategies/jwt-strategy';
import { UserRepository } from '../infrastructure/user.repository';
import { AuthService } from '../auth.service';
import { CryptoService } from '../../crypto/crypto.service';
import { RefreshTokenRepository } from '../infrastructure/refresh-token.repository';
import * as argon2 from 'argon2';

import Config from '../../config';

describe('AuthResolver', () => {
  let resolver: AuthResolver;
  let repository: UserRepository;

  const correctUser = {
    email: 'correct@tester.com',
    password: 'admin123',
  };

  const incorrectUser = {
    email: 'undefined@tester.com',
    password: 'admin123',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        EventEmitterModule.forRoot(),
        KnexModule.forRoot({ config: Config.Database }),
        JwtModule.register({
          secret: Config.Auth.jwtSecret,
          signOptions: {
            expiresIn: `${Config.Auth.jwtExpire}s`
          },
        }),
      ],
      providers: [
        CryptoService,
        AuthService,
        UserRepository,
        AuthResolver,
        RefreshTokenRepository,
        JwtStrategy,
      ],
    }).compile();

    resolver = module.get<AuthResolver>(AuthResolver);
    repository = module.get<UserRepository>(UserRepository);

    await repository.db('user')
      .insert({
        ...correctUser,
        name: "tester",
        password: await argon2.hash(correctUser.password),
      });
    await repository.db('user').where('email', incorrectUser.email).del();
  });

  afterEach(async () => {
    await repository.db('user').where('email', correctUser.email).del();
  });

  describe('login', () => {
    it('resolver should be defined', () => {
      expect(resolver).toBeDefined();
    });

    it('should return an auth token', async () => {
      const response = resolver.login(correctUser, { res: { cookie: (_par1: any, _par2: any, _par3: any) => null } });

      expect((await response).jwtToken).toBeDefined();
      expect((await response).refreshToken).toBeDefined();
    });
  });
});
