export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  confirmedAt: Date;
  updatedAt: Date;
  createdAt: Date;
}