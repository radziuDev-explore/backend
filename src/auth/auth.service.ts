import * as argon2 from 'argon2';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from 'src/user/interface/user.interface';
import { UserRepository } from './infrastructure/user.repository';
import { RefreshTokenRepository } from './infrastructure/refresh-token.repository';
import { LoginUserInput } from './dto/login-user.input';
import { AuthTokenDto } from './dto/auth-token.dto';
import { JwtService } from '@nestjs/jwt';
import { CryptoService } from '../crypto/crypto.service';
import { addMilliseconds, isAfter } from 'date-fns';

import Config from '../config'

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userRepository: UserRepository,
    private readonly cryptoService: CryptoService,
    private readonly refreshTokenRepository: RefreshTokenRepository,
  ) {}

  async createAuthTokens(user: Partial<User>): Promise<AuthTokenDto> {
    const refreshToken = this.cryptoService.generateRefreshToken(Config.Auth.refreshSecret);

    const accessTokenExpireDate = addMilliseconds(new Date(), Config.Auth.jwtExpire * 1000);
    const refreshTokenExpireDate = addMilliseconds(new Date(), Config.Auth.refreshExpire * 1000);

    await this.refreshTokenRepository.persist({
      userId: user.id,
      token: refreshToken,
      expiresAt: refreshTokenExpireDate,
    });

    return {
      jwtToken: {
        token: this.jwtService.sign({
          email: user.email,
          sub: user.id,
        }),
        expiresAt: accessTokenExpireDate,
        cookieOptions: {
          httpOnly: false,
          sameSite: true,
          expires: accessTokenExpireDate,
        },
      },
      refreshToken: {
        token: refreshToken,
        expiresAt: refreshTokenExpireDate,
        cookieOptions: {
          httpOnly: true,
          sameSite: true,
          expires: refreshTokenExpireDate,
        },
      },
    };
  }

  async checkRefreshTokenSubject(token: string): Promise<Partial<User>> {
    const refreshToken = await this.refreshTokenRepository.findOneByToken(token);

    if (!refreshToken) {
      throw new UnauthorizedException('Refresh token invalid.');
    }

    if (isAfter(new Date(), refreshToken.expiresAt)) {
      throw new UnauthorizedException('Refresh token expired.');
    }

    const user = await this.userRepository.findOneById(refreshToken.userId);
    if (!user) throw new UnauthorizedException();
    
    const { password: userPass, ...result } = user;
    return result;
  }

  async revokeRefreshToken(token: string): Promise<void> {
    await this.refreshTokenRepository.delete(token);
  }
    
  async validateUser({ email, password }: LoginUserInput): Promise<Partial<User>> {
    const user = await this.userRepository.findOneByEmail(email);
    if (!user) throw new UnauthorizedException();
    
    const isPasswordValid = await argon2.verify(user.password, password);
    if (!isPasswordValid) throw new UnauthorizedException();

    const { password: userPass, ...result } = user;
    return result;
  }
}
