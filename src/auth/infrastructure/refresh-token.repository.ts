import { Injectable, Logger } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { RefreshToken } from '../interface/refresh-token.interface';

@Injectable()
export class RefreshTokenRepository {
  private readonly logger = new Logger(RefreshTokenRepository.name);

  constructor(
    @InjectKnex() private readonly db: Knex,
  ) {}

  async persist(refreshToken: RefreshToken): Promise<void> {
    try {
      await this.db('refresh_token')
        .insert({
          userId: refreshToken.userId,
          token: refreshToken.token,
          expiresAt: refreshToken.expiresAt,
        });
    } catch (error) {
      this.logger.error('An error occured while trying to save refresh token: ', error);
    }
  }

  async findOneByToken(token: string): Promise<RefreshToken | null> {
    try {
      const refreshToken = await this.db('refresh_token')
        .select('userId', 'token', 'expiresAt')
        .where('token', token)
        .first();
      return refreshToken || null;
    } catch (error) {
      return null; 
    }
  }

  async delete(token: string): Promise<void> {
    await this.db('refresh_token')
      .delete()
      .where('token', token);
  }
}
