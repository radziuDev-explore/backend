import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { User } from '../interface/user.interface';

@Injectable()
export class UserRepository {
  constructor(
    @InjectKnex() public readonly db: Knex,
  ) {}

  async findOneByEmail(email: string): Promise<User | null> {
    const user = await this.db('user')
      .where('email', email)
      .first();

    return user || null;
  }

  async findOneById(id: number): Promise<User | null> {
    const user = await this.db('user')
      .where('id', id)
      .first();

    return user || null;
  }
}
