import { Module } from '@nestjs/common';
import { TagModule } from './tag/tag.module';
import { ScrapTagModule } from './scrap-tag/scrap-tag.module';
import { OfferModule } from './offer/offer.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { KnexModule } from 'nestjs-knex';
import { WalletModule } from './wallet/wallet.module';
import { GraphQLModule } from '@nestjs/graphql';
import { UiModule } from './ui/ui.module';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { CryptoModule } from './crypto/crypto.module';

import Config from './config';

@Module({
  imports: [
    KnexModule.forRoot({ config: Config.Database }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: true,
      sortSchema: true,
      playground: true,
      installSubscriptionHandlers: true,
      path: '/graphql',
      context: ({ req, res }) => ({ req, res }),
      formatError: (error) => {
        if (error?.extensions?.originalError) {
          const exError: any = error.extensions.originalError;
          return  exError ;
        }
        return error;
      },
    }),
    EventEmitterModule.forRoot(),
    TagModule,
    ScrapTagModule,
    OfferModule,
    WalletModule,
    UiModule,
    UserModule,
    AuthModule,
    CryptoModule,
  ],
})

export class AppModule {}
