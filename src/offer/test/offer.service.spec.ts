require('dotenv').config({ path: `${process.cwd()}/.env` });

import { EventEmitterModule } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { OfferService } from '../offer.service';
import { OfferRepository } from '../infrastructure/offer.repository';
import { KnexModule } from 'nestjs-knex';
import { ServicesTracked } from '../../tag/enum/services-tracked.enum';
import { Tag } from '../../tag/tag';

import Config from '../../config';

describe('OfferService', () => {
  let service: OfferService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        EventEmitterModule.forRoot(),
        KnexModule.forRoot({ config: Config.Database }),
      ],
      providers: [OfferService, OfferRepository],
    }).compile();

    service = module.get<OfferService>(OfferService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should insert new offers', () => {
    const tag = new Tag({
      id: 1,
      userId: 1,
      userTagId: 117,
      name: 'test',
      quantityOfPages: 2,
      services: [ServicesTracked.AMAZON],
      createdAt: new Date(),
      updatedAt: new Date(),
    })

    service.insertOffers(tag, []);
    expect(service).toBeDefined();
  });
});
