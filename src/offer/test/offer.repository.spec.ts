require('dotenv').config({ path: `${process.cwd()}/.env` });

import { Test, TestingModule } from '@nestjs/testing';
import { KnexModule } from 'nestjs-knex';
import { OfferRepository } from '../infrastructure/offer.repository';

import Config from '../../config';

describe('OfferRepository', () => {
  let repository: OfferRepository;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        KnexModule.forRoot({ config: Config.Database }),
      ],
      providers: [
        OfferRepository,
      ],
    }).compile();

    repository = module.get<OfferRepository>(OfferRepository);
  });


  it('should create an offer', async () => {
    const tagId = 1;
    const offerData = {
      title: 'Test Offer',
      link: 'http://test.com',
      price: 1099,
    };

    const expectedOffer = {
      id: expect.any(Number),
      tagId,
      ...offerData,
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
    };
  
    const createdOffer = await repository.createOffer(tagId, offerData);
    repository.db('offer').where('id', createdOffer.id).delete();

    expect(createdOffer).toEqual(expectedOffer);
  });
});