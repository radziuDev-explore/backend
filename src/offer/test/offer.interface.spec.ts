import { Offer } from '../interface/offer.interface';

describe('Offer object', () => {
  it('should be defined', () => {
    const offer = new Offer();
    expect(offer).toBeDefined()
  });

  it('should have all required fields', () => {
    const offer: Offer = {
      id: 1,
      tagId: 1,
      title: 'Sample Offer',
      link: 'https://example.com',
      price: 10.0,
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    
    expect(offer.id).toBeDefined();
    expect(typeof offer.id).toBe('number');
    expect(offer.tagId).toBeDefined();
    expect(typeof offer.tagId).toBe('number');
    expect(offer.title).toBeDefined();
    expect(typeof offer.title).toBe('string');
    expect(offer.link).toBeDefined();
    expect(typeof offer.link).toBe('string');
    expect(offer.price).toBeDefined();
    expect(typeof offer.price).toBe('number');
    expect(offer.createdAt).toBeDefined();
    expect(offer.createdAt instanceof Date).toBeTruthy();
    expect(offer.updatedAt).toBeDefined();
    expect(offer.updatedAt instanceof Date).toBeTruthy();
  });
});