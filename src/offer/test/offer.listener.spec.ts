import { Test, TestingModule } from '@nestjs/testing';
import { OfferService } from '../offer.service';
import { Logger } from '@nestjs/common';
import { TagScrapedListener } from '../listener/tag-scraped.listener';
import { Tag } from '../../tag/tag';
import { ServicesTracked } from '../../tag/enum/services-tracked.enum';

describe('TagScrapedListener', () => {
  let listener: TagScrapedListener;
  let offerService: OfferService;

  const mockedPayload = {
    tag: new Tag({
      id: 1,
      userId: 1,
      userTagId: 117,
      name: 'test',
      quantityOfPages: 2,
      services: [ServicesTracked.AMAZON],
      createdAt: new Date(),
      updatedAt: new Date(),
    }),
    scrapedOffers: [
      {
        title: 'Test Offer 1',
        link: 'http://testoffer1.com',
        price: 10,
      },
      {
        title: 'Test Offer 2',
        link: 'http://testoffer2.com',
        price: 20,
      },
    ],
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TagScrapedListener,
        {
          provide: OfferService,
          useValue: {
            insertOffers: jest.fn(),
          },
        },
        {
          provide: Logger,
          useValue: {
            log: jest.fn(),
            error: jest.fn(),
          },
        },
      ],
    }).compile();

    offerService = module.get<OfferService>(OfferService);
    listener = new TagScrapedListener(offerService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should call OfferService.insertOffers with correct arguments', async () => {
    await listener.handleTagScrapedEvent(mockedPayload);

    expect(offerService.insertOffers).toHaveBeenCalledTimes(1);
    expect(offerService.insertOffers).toHaveBeenCalledWith(
      mockedPayload.tag,
      mockedPayload.scrapedOffers,
    );
  });
});