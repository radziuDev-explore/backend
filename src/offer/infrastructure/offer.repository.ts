import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { Offer } from '../interface/offer.interface';
import { Tag } from 'src/tag/tag';

@Injectable()
export class OfferRepository {
  constructor(
    @InjectKnex() public readonly db: Knex,
  ) { }

  async createOffer(tagId: number, offer: Partial<Offer>): Promise<Offer> {
    if (offer.title.length > 499) {
      console.log('?')
      offer.title = offer.title.substring(0, 499);
    }
    const [createdOffer] = await this.db('offer').insert({ tagId, ...offer }).returning('*');
    return createdOffer;
  }

  async updateUserTagId(tag: Tag): Promise<void> {
    await this.db('userTag')
      .update('updatedAt', new Date())
      .where('id', tag.snapshot().userTagId);
  }
}