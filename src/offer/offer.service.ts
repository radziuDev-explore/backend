import { Injectable } from '@nestjs/common';
import { Offer } from './interface/offer.interface';
import { OfferRepository } from './infrastructure/offer.repository';
import { Tag } from 'src/tag/tag';

@Injectable()
export class OfferService {
  constructor(
    private offerRepository: OfferRepository,
  ) {}
  public async insertOffers(tag: Tag, offers: Partial<Offer>[]) {
    for(let i = 0; i < offers.length; i += 1) {
      await this.offerRepository.createOffer(tag.snapshot().id, offers[i]);
    }

    await this.offerRepository.updateUserTagId(tag);
  }
}
