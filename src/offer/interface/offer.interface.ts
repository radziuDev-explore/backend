import { ObjectType, Field  } from '@nestjs/graphql';
@ObjectType()
export class Offer {
  @Field()
  id: number;

  @Field()
  tagId: number;

  @Field()
  title: string;

  @Field()
  link: string;

  @Field()
  price: number;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}