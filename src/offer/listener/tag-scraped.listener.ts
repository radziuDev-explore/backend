import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { TagScrapedEvent } from '../../scrap-tag/event/tag-scraped.event';
import { OfferService } from '../offer.service';

@Injectable()
export class TagScrapedListener {
  public readonly logger = new Logger(TagScrapedListener.name);

  constructor(
    private readonly offerService: OfferService,
  ) {}

  @OnEvent('scrap-tag.fetched')
  async handleTagScrapedEvent(payload: TagScrapedEvent) {
    try {
      await this.offerService.insertOffers(payload.tag, payload.scrapedOffers);
      this.logger.log('Inserting offers.');
    } catch (error) {
      this.logger.error('Insert offers failed. Error: ', error);
    }
  }
}
