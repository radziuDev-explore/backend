import { Module } from '@nestjs/common';
import { OfferService } from './offer.service';
import { TagScrapedListener } from './listener/tag-scraped.listener';
import { OfferRepository } from './infrastructure/offer.repository';

@Module({
  providers: [
    OfferService,
    TagScrapedListener,
    OfferRepository,
  ],
})
export class OfferModule {}
