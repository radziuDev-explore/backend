require('dotenv').config({ path: `${process.cwd()}/.env` });
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import * as cookieParser from 'cookie-parser';

import Config from 'src/config';

async function bootstrap() {
  const { 
    App: { port, host, prefix },
    Logger: { levels },
  } = Config;

  const app = await NestFactory.create(AppModule, { logger: levels });

  app.enableCors({
    origin: ['http://127.0.0.1:3000'],
    methods: ['*'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,
  });

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    }),
  );

  app.use(cookieParser());

  app.setGlobalPrefix(prefix);
  await app.listen(port, host);
}
bootstrap();
