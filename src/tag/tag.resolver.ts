import { Resolver, Args, Mutation, Context } from '@nestjs/graphql';
import { SubscribeToTagInput } from './dto/subscribe-to-tag.input';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { InternalServerErrorException, Logger, NotFoundException, UseGuards } from '@nestjs/common';
import { TagService } from './tag.service';
import { TagDto } from './dto/tag.dto';
import { Tag } from './interface/tag.interface';
import { UnsubscribeDto } from './dto/unsubscribe.dto';
import { UnsubscribeFromTagInput } from './dto/unsubscribe-from-tag.input';
import { ScrapTagDto } from './dto/scrap-tag.dto';
import { ScrapTagInput } from './dto/scrap-tag.input';
import { TagDoesNotExistException } from './exception/tag-does-not-exist.exception';
import { InsufficientFundException } from 'src/wallet/exception/insufficient-fund.exception';

@Resolver()
export class TagResolver {
  private readonly logger = new Logger(TagResolver.name);

  constructor(
    private tagService: TagService,
  ) {}

  @Mutation(() => TagDto)
  @UseGuards(JwtAuthGuard)
  async subscribeToTag(
    @Context('req') { user }: { user: { id: number, email: string } },
    @Args('subscribeToTagInput') subscribeToTagInput: SubscribeToTagInput
  ): Promise<Tag> {
    try {
      const tag = await this.tagService.subscribeToTag(
        user.id,
        subscribeToTagInput.tagName,
        subscribeToTagInput.quantityOfPages,
        subscribeToTagInput.services,
      );
      
      return tag.snapshot();
    } catch (error) {
      this.logger.error(`An error occured while scrap tag: `, error);
      throw new InternalServerErrorException('Unknown error occurred, try again.');
    }
  }

  @Mutation(() => UnsubscribeDto)
  @UseGuards(JwtAuthGuard)
  async unsubscribeFromTag(
    @Context('req') { user }: { user: { id: number, email: string } },
    @Args('unsubscribeFromTagInput') unsubscribeFromTagInput: UnsubscribeFromTagInput,
  ): Promise<UnsubscribeDto> {
    try {
      await this.tagService.unsubscribeFromTag(
        user.id,
        unsubscribeFromTagInput.userTagId
      );

      return {
        message: 'Successfully unsubscribed to the tag.',
        userId: user.id,
        userTagId: unsubscribeFromTagInput.userTagId,
      }
    } catch (error) {
      switch (error.constructor) {
        case TagDoesNotExistException:
          throw new NotFoundException('The specified tag does not exist.')
        default:
          this.logger.error(`An error occured while scrap tag: `, error);
          throw new InternalServerErrorException('Unknown error occurred, try again.');
      }
    }
  }

  @Mutation(() => ScrapTagDto)
  @UseGuards(JwtAuthGuard)
  async scrapTag(
    @Context('req') { user }: { user: { id: number, email: string } },
    @Args('scrapTagInput') scrapTagInput: ScrapTagInput,
  ): Promise<ScrapTagDto> {
    try {
      await this.tagService.scrapTagForUser(
        user.id,
        scrapTagInput.userTagId,
      );

      return {
        message: 'Tag scraping ordered.',
        userId: user.id,
        userTagId: scrapTagInput.userTagId,
      }
    } catch (error) {
      switch (error.constructor) {
        case TagDoesNotExistException:
          throw new NotFoundException('The specified tag does not exist.')
        case InsufficientFundException:
          throw new NotFoundException('You do not have enough funds to perform this operation.')
        default:
          this.logger.error(`An error occured while scrap tag: `, error);
          throw new InternalServerErrorException('Unknown error occurred, try again.');
      }
    }
  }
}