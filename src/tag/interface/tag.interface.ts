import { ServicesTracked } from "../enum/services-tracked.enum";
export interface Tag {
  id: number;
  userId: number;
  userTagId: number;
  name: string;
  services: ServicesTracked[];
  quantityOfPages: number;
  price: number;
  updatedAt: Date;  
  createdAt: Date;
}
