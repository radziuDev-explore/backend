import { Injectable } from '@nestjs/common';
import { TagRepository } from './infrastructure/tag.repository';
import { AssignTagRepository } from './infrastructure/assign-tag.repository';
import { ServicesTracked } from './enum/services-tracked.enum';
import { WalletService } from '../wallet/wallet.service';
import { WalletTransactionLocation } from '../wallet/enum/wallet-transaction-location.enum';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { StartScrapingEvent } from './event/start-scraping.event';
import { Tag } from './tag';
import { TagDoesNotExistException } from './exception/tag-does-not-exist.exception';

@Injectable()
export class TagService {
  constructor(
    private tagRepository: TagRepository,
    private assignTagRepository: AssignTagRepository,
    private walletService: WalletService,
    private eventEmitter: EventEmitter2,
  ) { }
  
  private async getTag(tagName: string): Promise<Tag> {
    const tag = await this.tagRepository.findTagByTagName(tagName);
    if (tag) return tag;

    return await this.tagRepository.createTag({ tag: tagName });
  }

  public async subscribeToTag(
    userId: number,
    tagName: string,
    quantityOfPages: number,
    services: ServicesTracked[],
  ): Promise<Tag> {
    const tag = await this.getTag(tagName);
    await this.assignTagRepository.assignTag(
      tag,
      userId,
      quantityOfPages,
      { services },
    );

    return tag;
  }

  public async unsubscribeFromTag(
    userId: number,
    userTagId: number,
  ): Promise<void> {
    await this.assignTagRepository.removeTagFromUser(userId, userTagId);
  }

  public async scrapTagForUser(
    userId: number,
    userTagId: number,
  ): Promise<void> {
    const tag = await this.assignTagRepository.findAssignedTagById(userId, userTagId);

    if (!tag) {
      throw new TagDoesNotExistException();
    }

    await this.walletService.subtract(
      userId,
      tag.snapshot().price,
      WalletTransactionLocation.SCRAP_TAG, 
      { 
        name: `Tag #${tag.snapshot().id} scraping for user #${userId}`,
        tag: tag.snapshot(),
      },
    );
    
    this.eventEmitter.emit('scrap-tag.start',
      new StartScrapingEvent(tag),
    );
  }
}
