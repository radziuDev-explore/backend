require('dotenv').config({ path: `${process.cwd()}/.env` });

import { Test, TestingModule } from '@nestjs/testing';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { KnexModule } from 'nestjs-knex';
import { WalletModule } from '../../wallet/wallet.module';

import { TagService } from '../tag.service';
import { TagRepository } from '../infrastructure/tag.repository';
import { AssignTagRepository } from '../infrastructure/assign-tag.repository';
import { ServicesTracked } from '../enum/services-tracked.enum';
import * as argon2 from 'argon2';

import Config from '../../config';

describe('TagService', () => {
  let service: TagService;
  let repository: AssignTagRepository;
  let tagId = null;
  let userId = null;

  const correctUser = {
    email: 'correct@tester.com',
    password: 'admin123',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        EventEmitterModule.forRoot(),
        KnexModule.forRoot({ config: Config.Database }),
        WalletModule,
      ],
      providers: [
        TagService,
        TagRepository,
        AssignTagRepository,
      ],
    }).compile();

    service = module.get<TagService>(TagService);
    repository = module.get<AssignTagRepository>(AssignTagRepository);

   const tempUser: any[] = await repository.db('user')
      .insert({
        ...correctUser,
        name: "tester",
        password: await argon2.hash(correctUser.password)
      }).returning('*');

   userId = tempUser[0].id;
  });

  afterEach(async() => {
    await repository.db('user').where('email', correctUser.email).del();
  });

  it('Service should be defined', () => {
    expect(service).toBeDefined();
  });

  it('Assign tag', async() => {
    const tag = await service.subscribeToTag(userId,'test tag 123', 1, [ ServicesTracked.ALLEGRO ]);
    tagId = tag.snapshot().id;

    expect(tagId).not.toBeNull();
  });

   it('Remove tag from user', async() => {
    const tag = await service.subscribeToTag(userId,'test tag 123', 1, [ ServicesTracked.ALLEGRO ]);
    const current = tag.snapshot().userTagId;

    const res = await service.unsubscribeFromTag(userId, current);
    expect(res).toBeUndefined();
  });
});
