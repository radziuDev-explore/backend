import { Tag } from '../tag';
import { ServicesTracked } from '../enum/services-tracked.enum';

describe('TagClass', () => {
  it('Tag should be created', () => {
    const yesterday = new Date(new Date().getTime() - (24 * 60 * 60 * 1000));
    const testTag = new Tag({
      id: 1,
      userId: null,
      userTagId: null,
      name: 'Karta graficzna',
      services: [ServicesTracked.ALLEGRO],
      quantityOfPages: 2,
      createdAt: yesterday,
      updatedAt: yesterday,
    });

    const snapshot = testTag.snapshot();
    expect(snapshot).toBeDefined();
  });


  it('Tag should return correct price (2 pages)', async() => {
    const yesterday = new Date(new Date().getTime() - (24 * 60 * 60 * 1000));
    const testTag = new Tag({
      id: 1,
      userId: null,
      userTagId: null,
      name: 'Karta graficzna',
      services: [ServicesTracked.ALLEGRO],
      quantityOfPages: 2,
      createdAt: yesterday,
      updatedAt: yesterday,
    });

    const snapshot = testTag.snapshot();
    expect(snapshot.price).toBe(48);
  });

  it('Tag should return correct price (null pages)', async() => {
    const yesterday = new Date(new Date().getTime() - (12 * 60 * 60 * 1000));
    const testTag = new Tag({
      id: 1,
      userId: null,
      userTagId: null,
      name: 'Karta graficzna',
      services: [ServicesTracked.ALLEGRO],
      quantityOfPages: null,
      createdAt: yesterday,
      updatedAt: yesterday,
    });

    const snapshot = testTag.snapshot();
    expect(snapshot.price).toBe(36);
  });
});
