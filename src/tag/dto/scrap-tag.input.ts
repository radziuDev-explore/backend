import { InputType, Field, Int } from '@nestjs/graphql';
import { IsNotEmpty, IsNumber } from 'class-validator';

@InputType()
export class ScrapTagInput {
  @Field(() => Int)
  @IsNotEmpty({ message: 'Field \'userTagId\' should not be empty.' })
  @IsNumber({ maxDecimalPlaces: 0, allowInfinity: false, allowNaN: false }, { message: 'Field \'userTagId\' should be number.' })
  userTagId: number;
}
