import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class UnsubscribeDto {
  @Field()
  userId: number;
  
  @Field()
  userTagId: number;
  
  @Field()
  message: string;
}
