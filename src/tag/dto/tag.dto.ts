import { ServicesTracked } from '../enum/services-tracked.enum';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class TagDto {
  @Field()
  id: number;
  
  @Field()
  userId: number;
  
  @Field()
  userTagId: number;

  @Field()
  name: string;

  @Field(() => [ServicesTracked])
  services: ServicesTracked[];

  @Field()
  quantityOfPages: number;

  @Field()
  price: number;
  
  @Field()
  updatedAt: Date;
  
  @Field()
  createdAt: Date;
}
