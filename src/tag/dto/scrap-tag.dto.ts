import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class ScrapTagDto {
  @Field()
  userId: number;
  
  @Field()
  userTagId: number;
  
  @Field()
  message: string;
}
