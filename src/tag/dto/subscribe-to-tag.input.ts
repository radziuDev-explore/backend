import { InputType, Field, Int } from '@nestjs/graphql';
import { IsArray, IsEnum, IsInt, IsNotEmpty, IsString, Length, Max, Min } from 'class-validator';
import { ServicesTracked } from '../enum/services-tracked.enum';

@InputType()
export class SubscribeToTagInput {
  @Field()
  @IsNotEmpty({ message: 'Field \'tagName\' should not be empty.' })
  @IsString({ message: 'Field \'tagName\' should be string.' })
  @Length(3, 50, { message: 'Field \'tagName\' should be between 3 and 50 characters long.' })
  tagName: string;

  @Field(() => Int)
  @IsNotEmpty({ message: 'Field \'quantityOfPages\' should not be empty.' })
  @IsInt({ message: 'Field \'quantityOfPages\' should be an integer.' })
  @Min(1, { message: 'Field \'quantityOfPages\' should be greater than or equal to 1.' })
  @Max(10, { message: 'Field \'quantityOfPages\' should be less than or equal to 10.' })
  quantityOfPages: number;

  @Field(() => [ServicesTracked])
  @IsNotEmpty({ message: 'Field \'services\' should not be empty.' })
  @IsArray({ message: 'Field \'services\' should be an array.' })
  @IsEnum(ServicesTracked, { each: true })
  services: ServicesTracked[];
}
