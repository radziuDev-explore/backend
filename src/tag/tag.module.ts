import { Module } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { TagService } from './tag.service';
import { AssignTagRepository } from './infrastructure/assign-tag.repository';
import { TagRepository } from './infrastructure/tag.repository';
import { WalletModule } from '../wallet/wallet.module';
import { TagResolver } from './tag.resolver';

@Module({
  imports: [
    WalletModule,
  ],
  providers: [
    EventEmitterModule,
    TagService,
    TagRepository,
    AssignTagRepository,
    TagResolver,
  ],
})

export class TagModule {}
