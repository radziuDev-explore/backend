import { ServicesTracked } from './enum/services-tracked.enum';

type TagProps = {
  id: number;
  userId: number;
  userTagId: number;
  name: string;
  services: ServicesTracked[];
  quantityOfPages: number;
  updatedAt: Date;
  createdAt: Date;
};

export class Tag {
  private id: number;
  private userId: number;
  private userTagId: number;
  private name: string;
  private services: ServicesTracked[];
  private quantityOfPages: number;
  private price: number;
  private updatedAt: Date;
  private createdAt: Date;

  constructor(props: TagProps) {
    this.id = props.id;
    this.userId = props.userId;
    this.userTagId = props.userTagId;
    this.name = props.name;
    this.services = props.services;
    this.quantityOfPages = props.quantityOfPages || 1;
    this.updatedAt = props.updatedAt || new Date();
    this.createdAt = props.createdAt || new Date();

    this.setPrice();
  }

  private setPrice() {
    const hours = Math.floor(Math.abs(this.updatedAt.valueOf() - new Date().valueOf()) / (60 * 60 * 1000));
    this.price = Math.max(48 - hours, 1) * this.quantityOfPages;
  }

  public assignTagToUser(
    userId: number,
    userTagId: number,
    quantityOfPages: number,
    services: ServicesTracked[],
    updatedAt: Date,
    createdAt: Date,
  ) {
    this.userId = userId;
    this.userTagId = userTagId;
    this.quantityOfPages = quantityOfPages;
    this.services = services;
    this.updatedAt = updatedAt;
    this.createdAt = createdAt;

    this.setPrice();
  }

  public snapshot() {
    return {
      id: this.id,
      userId: this.userId,
      userTagId: this.userTagId,
      name: this.name,
      services: this.services,
      quantityOfPages: this.quantityOfPages,
      price: this.price,
      updatedAt: this.updatedAt,
      createdAt: this.createdAt,
    };
  }
}