import { Tag } from '../tag';

export class StartScrapingEvent {
  constructor (
    public readonly tag: Tag,
  ) { }
};