import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { Tag } from '../tag';

@Injectable()
export class TagRepository {
  constructor(
    @InjectKnex() private readonly db: Knex,
  ) {}

  async findTagByTagName(tagName: string): Promise<Tag> {
    const tag = await this.db('tag')
      .where('tag', tagName)
      .first();

    if (!tag) return null;

    return new Tag({
      id: tag.id,
      userId: null,
      userTagId: null,
      name: tag.tag,
      services: null,
      quantityOfPages: null,
      updatedAt: null,
      createdAt: null,
    });
  }

  async createTag(createdTag: { tag: string }): Promise<Tag> {
    const [{ id }] = await this.db('tag')
      .insert(createdTag)
      .onConflict('tag')
      .ignore()
      .returning('id');

      return new Tag({
        id,
        userId: null,
        userTagId: null,
        name: createdTag.tag,
        services: null,
        quantityOfPages: null,
        updatedAt: null,
        createdAt: null,
      });
  }
}
