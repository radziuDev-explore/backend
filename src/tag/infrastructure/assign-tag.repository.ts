import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { ServicesTracked } from '../enum/services-tracked.enum';
import { Tag } from '../tag';
import { TagDoesNotExistException } from '../exception/tag-does-not-exist.exception';

@Injectable()
export class AssignTagRepository {
  constructor(
    @InjectKnex() public readonly db: Knex,
  ) { }

  async findAssignedTagById(
    userId: number,
    userTagId: number,
  ): Promise<Tag> {
    const userTag = await this.db('userTag')
      .where('id', userTagId)
      .where('userId', userId)
      .select(
        'id',
        'userId',
        'tagId',
        'quantityOfPages',
        this.db.raw(`meta->>'services' AS services`),
        'updatedAt',
        'createdAt',
        )
      .first();

    if (!userTag) return null;

    const tag = await this.db('tag')
      .where('id', userTag.tagId)
      .first();

    return new Tag({
      id: tag.id,
      userId,
      userTagId: userTag.id,
      name: tag.tag,
      services: JSON.parse(userTag.services),
      quantityOfPages: userTag.quantityOfPages,
      updatedAt: userTag.updatedAt,
      createdAt: userTag.createdAt,
    })
  }

  async assignTag(
    tag: Tag,
    userId: number,
    quantityOfPages: number,
    meta: { services: ServicesTracked[] },
  ): Promise<Tag> {
    const [{ id }] = await this.db('userTag')
      .onConflict(['tagId', 'userId'])
      .merge()
      .insert({ userId, tagId: tag.snapshot().id, quantityOfPages, meta })
      .returning('id');
    
    tag.assignTagToUser(
      userId,
      id,
      quantityOfPages,
      meta.services,
      new Date(),
      new Date(),
    );

    return tag;
  }

  async removeTagFromUser(
    userId: number,
    userTagId: number,
  ): Promise<void> {
    const userTag = await this.db('userTag')
      .where({ id: userTagId, userId })
      .first();

    if (!userTag) throw new TagDoesNotExistException();

    await this.db('userTag')
      .where('id', userTag.id)
      .delete();
  }
}
