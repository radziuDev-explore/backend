import { registerEnumType } from '@nestjs/graphql';

export enum ServicesTracked {
  ALLEGRO = 'allegro',
  AMAZON = 'amazon',
};

registerEnumType(ServicesTracked, {
  name: 'ServicesTracked',
});