import { ServicesTracked } from '../../tag/enum/services-tracked.enum';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { ScrapTagService } from '../scrap-tag.service';
import { Test, TestingModule } from '@nestjs/testing';
import { Tag } from '../../tag/tag';

const yesterday = new Date(new Date().getTime() - (24 * 60 * 60 * 1000));
const testTag = new Tag({
  id: 1,
  userId: null,
  userTagId: null,
  name: 'Karta graficzna',
  services: [ServicesTracked.ALLEGRO],
  quantityOfPages: 2,
  createdAt: yesterday,
  updatedAt: yesterday,
});

describe('ScrapTagService', () => {
  let service: ScrapTagService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [EventEmitterModule.forRoot()],
      providers: [ScrapTagService],
    }).compile();

    service = module.get<ScrapTagService>(ScrapTagService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should open browser', async() => {
    // await service.scrapForTag(testTag);
    expect(service).toBeDefined();
  }, 20 * 1000);
});
