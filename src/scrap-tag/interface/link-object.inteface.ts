import { ServicesTracked } from 'src/tag/enum/services-tracked.enum';

export interface linkObject {
  page: string,
  service: ServicesTracked
}
