import { Tag } from "src/tag/tag";
import { Offer } from "../../offer/interface/offer.interface";

export class TagScrapedEvent {
  constructor(
    public readonly tag: Tag,
    public readonly scrapedOffers: Partial<Offer>[],
  ) {}
}
