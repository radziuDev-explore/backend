import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { StartScrapingEvent } from '../../tag/event/start-scraping.event';
import { ScrapTagService } from '../scrap-tag.service';

@Injectable()
export class StartScrapingListener {
  private readonly logger = new Logger(StartScrapingListener.name);

  constructor(
    private readonly scrapTagService: ScrapTagService,
  ) {}

  @OnEvent('scrap-tag.start')
  async handleUserCreatedEvent(payload: StartScrapingEvent) {
    try {
      await this.scrapTagService.scrapForTag(payload.tag);
      this.logger.log(`Scraping tag #${payload.tag.snapshot().userTagId} succes.`);
    } catch (error) {
      this.logger.error('Scraping tag failed. Error: ', error);
    }
  }
}
