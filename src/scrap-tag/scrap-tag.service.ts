import { Injectable, Logger } from '@nestjs/common';
import { Tag } from 'src/tag/tag';
import AmazonScenario from './scenarios/amazon.scenario';
import AllegroScenario from './scenarios/allegro.scenario';
import { linkObject } from './interface/link-object.inteface';
import { ServicesTracked } from 'src/tag/enum/services-tracked.enum';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CaptchaApperedException } from './exception/captcha-appeared.exception';
@Injectable()
export class ScrapTagService {
  private readonly logger = new Logger(ScrapTagService.name);

  constructor(
    private readonly eventEmitter: EventEmitter2,
  ) {}

  public async scrapForTag(tag: Tag) {
    const tagData = tag.snapshot();
    const pageLinks = await this.generateLinks(
      tagData.name,
      tagData.services,
      tagData.quantityOfPages,
    );

    for(let i = 0; i < pageLinks.length; i += 1) {
      try {
        const scenarioMatrix = {
          amazon: AmazonScenario,
          allegro: AllegroScenario,
        };
  
        const scenario = new scenarioMatrix[pageLinks[i].service](this.eventEmitter);
        await scenario.init(tag, pageLinks[i].page);
        await scenario.run();
      } catch (error) {
        switch (error.constructor) {
          case CaptchaApperedException:
            this.logger.error(`A captcha appeared while scraping the link: ${pageLinks[i].page}`);
            break;
          default:
            throw error;
        }
      }
    }
  }

  private async generateLinks(
    value: string,
    services: ServicesTracked[],
    quantityOfPages: number,
  ): Promise<linkObject[]> {
    const tagLinks: linkObject[] = [];
    services.forEach((service: ServicesTracked) => {     
      for(let i = 0; i < quantityOfPages; i += 1) {
        const servicesURL = {
          allegro: `https://allegro.pl/listing?${new URLSearchParams({ string: value, p: (i + 1).toString() }).toString()}`,
          amazon: `https://www.amazon.pl/s?${new URLSearchParams({ k: value, page: (i + 1).toString() }).toString()}`,
        };
        tagLinks.push({ page: servicesURL[service], service });
      }
    });

    return tagLinks;
  }
}
