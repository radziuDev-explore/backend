import Scenario from './scenario';
import { CaptchaApperedException } from '../exception/captcha-appeared.exception';

export default class AllegroScenario extends Scenario {
  async run() {
    let didCapthAppeared = await this.page.$$('div.captcha__human__title');
    if (didCapthAppeared) {
      await this.browser.close();
      throw new CaptchaApperedException();
    }

    // secound type of captcha
    didCapthAppeared = await this.page.$$('div.recaptchaContainer');
    if (didCapthAppeared) {
      await this.browser.close();
      throw new CaptchaApperedException();
    }

    await this.page.waitForSelector('section article');
    
    const offers = await this.page.evaluate(() => {
      let results = [];
      const items = document.querySelectorAll('section article');
      for (let i = items.length; i--; ) {
        const item = items[i]
        const link: HTMLElement  = item.querySelector('a[title]');
        const price: HTMLElement  = item.querySelector('span[aria-label]');
        
        if (!price || !link ) continue;

        results = [...results, {
          title: link?.innerHTML,
          link: link?.getAttribute('href'),
          price: (parseFloat(price?.getAttribute('aria-label')?.split(' zł')[0].replace(',', '.')) * 100).toFixed(0),
        }];
      }
      return results;
    });

    await this.browser.close();
    await this.emitScrapedOffers(offers);
  }
}