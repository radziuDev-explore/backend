import puppeteer, { Browser, Page } from 'puppeteer';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { TagScrapedEvent } from '../event/tag-scraped.event';
import { Offer } from '../../offer/interface/offer.interface';
import { Tag } from 'src/tag/tag';

import Config from '../../config';

export default abstract class Scenario {
  public page: Page;
  public browser: Browser;
  private tag: Tag;

  constructor(
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async init(tag: Tag, link: string) {
    const { headless, executablePath } = Config.Puppeteer;

    const puppeteerLaunchOptions = {
      headless,
      ...(executablePath ? { executablePath } : {}),
    };
    
    this.browser = await puppeteer.launch(puppeteerLaunchOptions);
    this.page = await this.browser.newPage();
    this.tag = tag;
    
    await this.page.goto(link);
  }

  async emitScrapedOffers(offers: Partial<Offer>[]) {
    this.eventEmitter.emit(
      'scrap-tag.fetched',
      new TagScrapedEvent(this.tag, offers),
    );
  }

  abstract run(): Promise<void>;
}