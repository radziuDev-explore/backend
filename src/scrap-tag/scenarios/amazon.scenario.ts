import Scenario from './scenario';

export default class AmazonScenario extends Scenario {
  async run() {
    await this.page.waitForSelector('div.s-result-item');

    const offers = await this.page.evaluate(() => {
      let results = [];
      const items = document.querySelectorAll('.s-result-item .s-card-border');
      for (let i = items.length; i--; ) {
        const item = items[i];
        const title: HTMLElement = item.querySelector('h2 > a > span');
        const price: HTMLElement = item.querySelector('.a-price-whole');
        const cents: HTMLElement = item.querySelector('.a-price-fraction');
        const link: HTMLElement = item.querySelector('a.a-link-normal');

        if (!title || !price || !link ) continue;

        results = [...results, {
          title: title.innerText,
          link: `https://www.amazon.pl/${link.getAttribute('href')}`,
          price: (parseFloat(`${price.innerText}.${cents.innerText}`) * 100).toFixed(0),
        }];
      }
      return results;
    });

    await this.browser.close();
    await this.emitScrapedOffers(offers);
  }
}