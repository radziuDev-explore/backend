import { Module } from '@nestjs/common';
import { ScrapTagService } from './scrap-tag.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { StartScrapingListener } from './listener/start-scraping.listener';

@Module({
  providers: [
    EventEmitterModule,
    ScrapTagService,
    StartScrapingListener,
  ],
})
export class ScrapTagModule {}
