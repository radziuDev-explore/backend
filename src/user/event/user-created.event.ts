export class UserCreatedEvent {
  constructor (
    public readonly User: { id: number },
  ) { }
};