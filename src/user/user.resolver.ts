import { Resolver, Mutation, Args } from '@nestjs/graphql';
import { UserService } from './user.service';
import * as argon2 from 'argon2';
import { CreateUserInput } from './dto/create-user.input';
import { User } from './dto/user.dto';
import { User as UserInterface } from './interface/user.interface';

@Resolver('User')
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Mutation(() => User)
  async createUser(
    @Args('createUserInput') payload: CreateUserInput,
  ): Promise<UserInterface> {
    const { name, email, password } = payload;

    const hashedPassword = await argon2.hash(password);
    const newUser = await this.userService.create({
      name,
      email,
      password: hashedPassword,
    });
    return newUser;
  }
}