import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { User } from '../interface/user.interface';

@Injectable()
export class UserRepository {
  constructor(
    @InjectKnex() private readonly db: Knex,
  ) {}

  async getUserByEmail(email: string)  {
    const User = await this.db('user')
    .where('email', email)
    .returning('*')
    .first();

   return User;
  }


  async save(user: Partial<User>): Promise<User> {
    const [User] = await this.db('user')
      .insert(user)
      .returning('*');

    return User;
  }
}
