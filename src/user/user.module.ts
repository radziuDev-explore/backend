import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { UserRepository } from './infrastructure/user.repository';
import { UserResolver } from './user.resolver';

@Module({
  providers: [
    EventEmitterModule,
    UserRepository,
    UserService,
    UserResolver,
  ],
})
export class UserModule {}
