import { InputType, Field } from '@nestjs/graphql';
import { Equals, IsBoolean, IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { Match } from 'src/decorator/match.decorator';

@InputType()
export class CreateUserInput {
  @Field()
  @IsNotEmpty({ message: "Field 'name' should not be empty." })
  @IsString({ message: "Field 'name' should be string." })
  @Length(3, 20, { message: "Field 'name' should be between 3 and 20 characters long." } )
  name: string;

  @Field()
  @IsNotEmpty({ message: "Field 'email' should not be empty." })
  @IsEmail({}, { message: "Field 'email' should be email." })
  email: string;

  @Field()
  @IsNotEmpty({ message: "Field 'password' should not be empty." })
  @IsString({ message: "Field 'password' should be string." })
  @Length(8, 40, { message: "Field 'password' should be between 8 and 40 characters long." } )
  password: string;

  @Field()
  @IsNotEmpty({ message: "Field 'confirmPassword' should not be empty." })
  @IsString({ message: "Field 'confirmPassword' should be string." })
  @Length(8, 40, { message: "Field 'confirmPassword' should be between 8 and 40 characters long." } )
  @Match('password', { message: "Field 'password' and 'confirmPassword' are not the same." })
  confirmPassword: string;

  @Field()
  @IsNotEmpty({ message: "Field 'agreedToPrivacyPolicy' should not be empty." })
  @IsBoolean({ message: "Field 'agreedToPrivacyPolicy' should be boolen." })
  @Equals(true, { message: "You must agree to the privacy policy." })
  agreedToPrivacyPolicy: boolean;
}