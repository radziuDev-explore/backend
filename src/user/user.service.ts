import { Injectable } from '@nestjs/common';
import { User } from './interface/user.interface';
import { UserRepository } from './infrastructure/user.repository';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { UserCreatedEvent } from './event/user-created.event';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async getUserByEmail(email: string): Promise<User> {
    return this.userRepository.getUserByEmail(email);
  }

  async create({ name, password, email }: Partial<User>): Promise<User> {
    const newUser = await this.userRepository.save({
      name, password, email,
    });

    this.eventEmitter.emit('user.created',
      new UserCreatedEvent({ id: newUser.id }),
    );

    return newUser;
  }
}