import { Module } from '@nestjs/common';
import { OfferResolver } from './offer.resolver';
import { TagResolver } from './tag.resolver';
import { UserResolver } from './user.resolver';

@Module({
  providers: [
    OfferResolver,
    TagResolver,
    UserResolver,
  ],
})

export class UiModule {}
