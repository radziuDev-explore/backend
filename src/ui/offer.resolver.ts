import { ForbiddenException, UseGuards } from '@nestjs/common';
import { Resolver, Query, Args, Context } from '@nestjs/graphql';
import { InjectKnex, Knex } from 'nestjs-knex';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { GetOffersInput } from './dto/get-offers.input';
import { OfferUiDto } from './dto/offer-ui.dto';

@Resolver()
export class OfferResolver {
  constructor(
    @InjectKnex() private readonly db: Knex,
  ) { }

  @Query(() => [OfferUiDto])
  @UseGuards(JwtAuthGuard)
  async getOffersByTag(
    @Context('req') { user }: { user: { id: number, email: string } },
    @Args('getOffersInput') getOffersInput: GetOffersInput,
  ): Promise<OfferUiDto[]> {
    const { tagId, updatedAt } = await this.db('userTag')
      .where('id', getOffersInput.userTagId)
      .where('userId', user.id)
      .first();

    if (!tagId) {
      throw new ForbiddenException('You have not assigned this tag.')
    }    

    const offers = await this.db('offer')
      .where('tagId', tagId)
      .where('createdAt', '<', new Date(updatedAt));

    const reducedOffers = offers.reduce((acc, { title, link, tagId, price, createdAt }) => {
      if (!acc[link]) {
        acc[link] = {
          title,
          link,
          tagId,
          price: [],
        };
      }
      acc[link].price.push({
        total: price,
        createdAt,
      });
      return acc;
    }, {});

    return Object.values(reducedOffers);
  }
}
