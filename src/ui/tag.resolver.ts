import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Context } from '@nestjs/graphql';
import { InjectKnex, Knex } from 'nestjs-knex';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { TagUiDto } from './dto/tag-ui.dto';
import { Tag as TagInterface } from 'src/tag/interface/tag.interface';
import { Tag } from 'src/tag/tag';

@Resolver()
export class TagResolver {
  constructor(
    @InjectKnex() private readonly db: Knex,
  ) { }

  @Query(() => [TagUiDto])
  @UseGuards(JwtAuthGuard)
  async getTagsForUser(
    @Context('req') { user }: { user: { id: number, email: string } },
  ): Promise<TagInterface[]> {
    const userTags = await this.db('userTag').where('userId', user.id);

    return Promise.all(userTags.map(async(userTag) => {
      const tag = await this.db('tag')
        .where('id', userTag.tagId)
        .first();

      const tempTag = new Tag({
        id: tag.id,
        userId: user.id,
        userTagId: userTag.id,
        name: tag.tag,
        services: userTag.meta.services,
        quantityOfPages: userTag.quantityOfPages,
        updatedAt: userTag.updatedAt,
        createdAt: userTag.createdAt,
      });

      return tempTag.snapshot();
    }));
  }
}
