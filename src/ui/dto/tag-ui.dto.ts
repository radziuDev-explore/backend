import { Field, ObjectType } from '@nestjs/graphql';
import { ServicesTracked } from 'src/tag/enum/services-tracked.enum';

@ObjectType()
export class TagUiDto {
  @Field()
  id: number;
  
  @Field()
  userId: number;
  
  @Field()
  userTagId: number;

  @Field()
  name: string;

  @Field(() => [ServicesTracked])
  services: ServicesTracked[];

  @Field()
  quantityOfPages: number;

  @Field()
  price: number;
  
  @Field()
  updatedAt: Date;
  
  @Field()
  createdAt: Date;
}
