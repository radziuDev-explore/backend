import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class UserUiDto {
  @Field()
  id: number;
  
  @Field()
  name: string;

  @Field()
  email: string;

  @Field({ nullable: true })
  balance: number | null;

  @Field({ nullable: true })
  confirmedAt: Date | null;
  
  @Field()
  updatedAt: Date;
  
  @Field()
  createdAt: Date;
}
