import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class OfferUiDto {
  @Field()
  title: string;

  @Field()
  link: string;

  @Field(() => Int)
  tagId: number;

  @Field(() => [PriceDto])
  price: PriceDto[];
}

@ObjectType()
export class PriceDto {
  @Field(() => Int)
  total: number;

  @Field(() => Date)
  createdAt: Date;
}