import { UseGuards } from '@nestjs/common';
import { Context, Query, Resolver } from '@nestjs/graphql';
import { InjectKnex, Knex } from 'nestjs-knex';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { UserUiDto } from './dto/user-ui.dto';

@Resolver()
export class UserResolver {
  constructor(
    @InjectKnex() private readonly db: Knex,
  ) { }

  @Query(() => UserUiDto)
  @UseGuards(JwtAuthGuard)
  async current(
    @Context('req') { user }: { user: { id: number, email: string } },
    @Context('req') x: any,
  ): Promise<UserUiDto> {

    const { points } = await this.db('wallet').where('userId', user.id).first();
    const result = await this.db('user')
      .select([
        'id',
        'name',
        'email',
        'confirmedAt',
        'updatedAt',
        'createdAt',
      ])
      .where('id', user.id)
      .first();

    return {
      ...result,
      balance: points,
    };
  }
}