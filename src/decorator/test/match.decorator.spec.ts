import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { Match } from '../match.decorator';

class TestClass {
  @Match('passwordConfirmation', { message: 'Passwords do not match' })
  password: string;
  passwordConfirmation: string;
}

describe('Match decorator', () => {
  it('should validate matching passwords', async () => {
    const testObject = plainToClass(TestClass, {
      password: 'password',
      passwordConfirmation: 'password',
    });
    const errors = await validate(testObject);

    expect(errors.length).toBe(0);
  });

  it('should throw an error when passwords do not match', async () => {
    const testObject = plainToClass(TestClass, {
      password: 'password',
      passwordConfirmation: 'wrongPassword',
    });
    const errors = await validate(testObject);

    expect(errors.length).toBe(1);
    expect(errors[0].constraints).toEqual({
      Match: 'Passwords do not match',
    });
  });
});
