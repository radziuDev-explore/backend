import App from './app';
import Database from './database';
import Logger from './logger';
import Auth from './auth';
import Puppeteer from './puppeteer';

export default {
  App,
  Database,
  Logger,
  Auth,
  Puppeteer,
};