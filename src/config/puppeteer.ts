export default {
  headless: process.env.PUPPETEER_HEADLESS === 'true',
  executablePath: <string>process.env.PUPPETEER_PATH || null,
};