export default {
  client: 'pg',
  debug: process.env.DB_LOGGING === 'true',
  connection: {
    host: process.env.DB_HOST || '127.0.0.1',
    port: +process.env.DB_PORT || 5432,
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    typeCast: (field, next) => {
      if (field.type === 'TINY') return field.string() === '1';
      return next();
    },
  },
  pool: {
    min: 0,
    max: 10,
  },
  migrations: {
    tableName: 'migrations',
    extension: 'ts',
  },
};