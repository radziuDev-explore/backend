export default {
  host: process.env.HOST || '127.0.0.1',
  port: +process.env.PORT || 8080,
  prefix: process.env.PREFIX || 'api',
  hostUrl: process.env.HOST_URL || 'http://127.0.0.1:8080',

  loadLocations: process.env.LOAD_LOCATIONS === 'true',
};