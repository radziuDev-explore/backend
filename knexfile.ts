require('dotenv').config({ path: `${process.cwd()}/.env` });
import KnexConfig from './src/config/database';
module.exports = KnexConfig;